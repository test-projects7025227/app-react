import { addition, substract, multiply, divide } from '../../src/helpers/operations'

describe('Test on helper operations', () => {

	test('should return the sum of 10 and 10', () => {
		expect(addition()).toBe(20);
	});

	test('should return the difference of 10 and 10', () => {
		expect(substract()).toBe(0);
	});

	test('should return the multiply of 10 and 10', () => {
		expect(multiply()).toBe(100);
	});

	test('should return the sum of 10 and 10', () => {
		expect(divide()).toBe(1);
	});
});