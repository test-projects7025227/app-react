import reactLogo from './assets/react.svg'
import './App.css'
import { addition, substract, multiply, divide } from './helpers/operations';

function App() {

  return (
    <>
      <div>
          <img src={reactLogo} className="logo react" alt="React logo" />
      </div>
      <h1>Hello world!</h1>
      <div>
        <span>{ addition() }</span>
        <span>{ substract() }</span>
        <span>{ multiply() }</span>
        <span>{ divide() }</span>
      </div>
    </>
  )
}

export default App
